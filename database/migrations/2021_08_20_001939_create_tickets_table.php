<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('history_id')->unsigned();
            $table->string('name');
            $table->string('comment')->nullable();
            $table->enum('status', ['ACTIVO','EN_PROCESO','FINALIZADO','CANCELADO'])->default('ACTIVO');
            $table->timestamps();
            $table->foreign('history_id')->references('id')->on('histories')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
