<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Company::create([
            'name' => 'Empresa Number One',
            'nit' => '111222333',
            'address' => 'Bogota, Colombia',
            'phone' => '3121233232',
            'email' => 'empresa1@pruebas.com',
        ]);
        \App\Models\Company::create([
            'name' => 'Empresa Number Two',
            'nit' => '444555666',
            'address' => 'Medellin, Colombia',
            'phone' => '3123212222',
            'email' => 'empresa2@pruebas.com',
        ]);
        \App\Models\Company::create([
            'name' => 'Empresa Number Three',
            'nit' => '777888999',
            'address' => 'Barranquilla, Colombia',
            'phone' => '3129878899',
            'email' => 'empresa3@pruebas.com',
        ]);
    }
}
