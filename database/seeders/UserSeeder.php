<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Hugo',
            'email' => 'hugosfines@gmail.com',
            'password' => Hash::make('12345678'),
            'company_id' => 1,
        ]);
        \App\Models\User::create([
            'name' => 'Luz',
            'email' => 'luzmfernandezp77@gmail.com',
            'password' => Hash::make('12345678'),
            'company_id' => 2,
        ]);
        \App\Models\User::create([
            'name' => 'Hulmary',
            'email' => 'pori@gmail.com',
            'password' => Hash::make('12345678'),
            'company_id' => 2,
        ]);
        \App\Models\User::create([
            'name' => 'Neymar',
            'email' => 'ney@gmail.com',
            'password' => Hash::make('12345678'),
            'company_id' => 3,
        ]);
    }
}
