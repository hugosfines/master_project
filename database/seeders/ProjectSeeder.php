<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Project::create([
            'company_id' => 1,
            'name' => 'Proyecto número 1 (Empresa 1)',
        ]);
        \App\Models\Project::create([
            'company_id' => 2,
            'name' => 'Proyecto número 2 (Empresa 2)',
        ]);
        \App\Models\Project::create([
            'company_id' => 2,
            'name' => 'Proyecto número 3 (Empresa 2)',
        ]);
        \App\Models\Project::create([
            'company_id' => 3,
            'name' => 'Proyecto número 4 (Empresa 3)',
        ]);
    }
}
