<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HistoryController;
use App\Http\Controllers\TicketController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Historias de usuario
Route::get('/historias', function() {
    return view('histories.index');
})->middleware('auth')->name('history');
Route::resource('/histories', HistoryController::class);
// Tickets
Route::get('/tickets', function() {
    return view('tickets.index');
})->name('ticket');
Route::resource('/ticket', TicketController::class);