<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'history_id' => 'required|integer|exists:histories,id',
            'name' => 'required|string|max:255',
            'comment' => 'required|string|max:255',
            'status' => 'required|string|in:ACTIVO,EN_PROCESO,FINALIZADO,CANCELADO',
        ]);
        $ticket = Ticket::create([
            'history_id' => $request->history_id,
            'name' => $request->name,
            'comment' => $request->comment,
            'status' => $request->status,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id>0) {
            return Ticket::orderBy('created_at', 'desc')
            ->where('history_id', $id)
            ->get();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $request->validate([
            'history_id' => 'required|integer|exists:histories,id',
            'name' => 'required|string|max:255',
            'comment' => 'required|string|max:255',
            'status' => 'required|string|in:ACTIVO,EN_PROCESO,FINALIZADO,CANCELADO',
        ]);
        $ticket->name = $request->name;
        $ticket->comment = $request->comment;
        $ticket->status = $request->status;
        $ticket->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
