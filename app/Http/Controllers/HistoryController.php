<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Models\History;

class HistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $projects = \App\Models\Project::select('id', 'name')
        ->where('company_id', auth()->user()->company_id)
        ->get();
        return response()->json(['projects' => $projects]);
    }

    public function show($id)
    {
        if($id>0) {
            return History::orderBy('created_at', 'desc')
            ->with('user:id,company_id,name', 'user.company:id,name', 'project:id,company_id,name', 'project.company:id,name')
            ->where('project_id', $id)
            ->get();
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'project_id' => 'required|integer|exists:projects,id',
            'name' => 'required|string|max:255',
        ]);
        DB::beginTransaction();
        try {
            $history = History::create([
                'project_id' => $request->project_id,
                'user_id' => auth()->user()->id,
                'name' => $request->name,
            ]);
            $ticket = \App\Models\Ticket::create([
                'history_id' => $history->id,
                'name' => 'Ticket de creación',
                'comment' => 'Ticket de inicio al crear nueva historia',
                'status' => 'ACTIVO',
            ]);
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'project_id' => 'required|integer|exists:projects,id',
            'name' => 'required|string|max:255',
        ]);
        $history = History::findOrFail($id);
        $history->name = $request->name;
        $history->update();
    }

    public function destroy($id)
    {
        $history = History::findOrFail($id);
        $history->delete();
    }
}
